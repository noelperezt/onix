<?php
namespace apps\Referidos\controllers;

use fw_Klipso\kernel\classes\abstracts\aController;
use fw_Klipso\kernel\engine\middleware\Request;
use fw_Klipso\applications\send_mail\SendMail;



class ReferidosController extends aController{

    /* Write the code of the methods that are associated with the url.
    Important the method must be public */
    public function __construct($app)
    {
        parent::__construct($app);
    }
    private function getCodigoReferir(){
        $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; //posibles caracteres a usar
        $numerodeletras=6; //numero de letras para generar el texto
        $cadena = ""; //variable para almacenar la cadena generada
        for($i=0;$i<$numerodeletras;$i++){
            $cadena .= substr($caracteres,rand(0,strlen($caracteres)),1); /*Extraemos 1 caracter de los caracteres
			entre el rango 0 a Numero de letras que tiene la cadena */
        }
        return $cadena;
    }
    private function setSolicitarRegalo($monto, $correo){
        
        try{
            $model_onix = $this->Model('Onix_referidos');
            $data = $model_onix->find('cantidad_onix');   
            if(intval($data['cantidad_onix']) <= 0){
                response_json(['message' => 'La campaña ha sido cerrada porque ya repartimos los 10 millones de Onix']);
                return false;
            }
            $model = $this->Model('Regalos');
            $model->save([
                'correo_electronico' => $correo,
                'monto_onix'  => $monto,
                'regalo_aplicado' => 0
            ]);
                    
            $onix_restantes = intval($data['cantidad_onix']) - intval($monto);

            $model->raw("update Onix_referidos set cantidad_onix = ".intval($onix_restantes)." ");
            return true;
        }catch (\PDOException $e){
            error_log($e->getMessage());
            response_json(['message' => 'Disculpe fue imposible registrarlo, por favor haganos saber de este problema']);
            return false;
        }
    }
    private function setGuardarReferido(Request $request){
        $model = $this->Model('Referidos');       
        $codigo_para_referir = $this->getCodigoReferir();
        try{
            $model->save([
                'correo_electronico' => $request->_post('correo'),
                'clave' => sha1(base64_encode($request->_post('pass1'))),
                'referido'  => $request->_post('codigo_referido'),
                'codigo_referir' => $codigo_para_referir
            ]);

            if($this->setSolicitarRegalo(60, $request->_post('correo'))){
                if(!empty( $request->_post('codigo_referido') )){
                    $data = $model->raw("select correo_electronico from referidos where referido = '".$request->_post('codigo_referido')."' ");
                    if (!empty($data["correo_electronico"]) )
                        $this->setSolicitarRegalo(15, $data['correo_electronico']);
                }

                #enviar correo de bienvenida y link de referido
                $service_mail = new SendMail();
                if(LANGUAGE == 'es'){
                    $asunto = 'Bienvenido a Onixcoin';
                    $correo = file_get_contents(BASE_DIR . 'apps/Referidos/templates/mail/mail-referido-es.twig');
                }else{
                    $asunto = 'Welcome to Onixcoin';
                    $correo = file_get_contents(BASE_DIR . 'apps/Referidos/templates/mail/mail-referido-en.twig');
                }


                $correo = str_replace('********', 'http://www.onixcoin.com?codigo_referido='.$codigo_para_referir, $correo);

                $service_mail->send([$request->_post('correo')], $asunto, $correo);
                response_json(['message' => $asunto]);
            }
            
            
        }catch (\PDOException $e){
            if( preg_match('/duplicate key value/', $e->getMessage()) ){
                $message = $e->getMessage();
            }else{
                if(LANGUAGE == 'es')
                    $message = 'Disculpe fue imposible registrarlo, por favor haganos saber de este problema';
                else
                    $message = 'Sorry it was impossible to register, please let us know about this problem';
            }

            error_log($e->getMessage() . $request->_post('correo') );
            response_json(['message' => $message]);
        }


    }
    public function registros(Request $request){
        if($request->isPost())
            $this->setGuardarReferido($request);

    }
}