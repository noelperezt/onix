<?php

namespace apps\Admin\models;

use fw_Klipso\kernel\classes\abstracts\aModels;
use fw_Klipso\kernel\engine\dataBase\Constrainst;
use fw_Klipso\kernel\engine\dataBase\DataType;
use fw_Klipso\kernel\engine\dataBase\TypeFields;

class Multimedia extends aModels
{
    private $prefix_model = 'blog';

    public function __fields__()
    {
        $field = [
            'multimedia_id' => DataType::FieldAutoField(),
            'nombre_archivo' => DataType::FieldString(80, true),
            'nombre_cifrado' => DataType::FieldString(80,true),            
            'tipo_archivo' => DataType::FieldChar(true,'I'),
            'autor_id' => DataType::FieldInteger(true),
            'fecha_creacion' => DataType::FieldDateTime(true, DefaultDateTimeNow()),
            
        ];
        return $field;
    }

    public function __setPrimary()
    {
        $pk = [
            'multimedia_id'
        ];
        return $pk;

    }

    public function __setUnique(){ }

    public function __foreignKey()
    {
        $fk = [
            'autor_id' => Constrainst::ForeignKey('Usuario', 'user_id', Constrainst::on_delete(false)),
        ];
        return $fk;


    }
    public function __getPrefix()
    {
        return $this->prefix_model;
    }
}