<?php
namespace apps\Website\controllers;

use fw_Klipso\applications\send_mail\SendMail;
use fw_Klipso\kernel\classes\abstracts\aController;
use fw_Klipso\kernel\engine\middleware\Request;

class WebsiteController extends aController{

    /* Write the code of the methods that are associated with the url.
    Important the method must be public */
    public function __construct($app)
    {
        parent::__construct($app);
    }
    public function home($request){
        if($request->isPost()){
            $this->setGuardarCorreo($request);

        }
    	$context = [
            'titulo' => 'Onix la nueva era financiera'
        ];

        if($request->_get('codigo_referido') != false)
            $context = array_merge($context, ['codigo_referido' => $request->_get('codigo_referido')]);

        if(LANGUAGE == 'es')
    	   $this->render("es/index", $context);
        else
            $this->render("en/index", $context);
    }
     public function sesion($request){
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];
        $this->render("sesion", $context);
    }
    public function registro($request){
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];
        $this->render("registro", $context);
    }
    public function info($request){

        #$mail = new SendMail();

        #$mail->send(['programador.angel@gmail.com'],'prueba desde el codigo de onix','peeeeendiente');

        
        if($request->isPost()){
            $this->setGuardarCorreo($request);

        }
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];
        if(LANGUAGE == 'es')
            $this->render("es/info", $context);
        else
            $this->render("en/info", $context);
    }
    public function descargar($request){
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];
        if(LANGUAGE == 'es')
            $this->render("es/download", $context);
        else
            $this->render("en/download", $context);

    }
     public function comercio($request){
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];
        $this->render("comercio", $context);
    }
     public function blog($request){
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];
        $this->render("blog", $context);
    }
     public function contacto($request){
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];
         if(LANGUAGE == 'es')
            $this->render("es/contacto", $context);
         else
             $this->render("en/contacto", $context);
    }
    public function pools($request){
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];
        $this->render("pools", $context);
    }
    public function exchanges($request){
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];
        $this->render("exchanges", $context);
    }
    public function software($request){
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];
        if(LANGUAGE == 'es')
            $this->render("es/software", $context);
        else
            $this->render("en/software", $context);
    }
    public function road($request){
        if($request->isPost()){
            $this->setGuardarCorreo($request);

        }
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];

        if(LANGUAGE == 'es')
            $this->render("es/road", $context);
        else
            $this->render("en/road", $context);
    }
    public function ficha($request){
        if($request->isPost()){
            $this->setGuardarCorreo($request);

        }
        $context = [
            'titulo' => 'Onix la nueva era financiera'
        ];

        if(LANGUAGE == 'es')
            $this->render("es/ficha", $context);
        else
            $this->render("en/ficha", $context);
    }
    
    private function setGuardarCorreo(Request $request){
        try{
            $this->Model('Suscripcion')->save([
                'correo_electronico' => $request->_post('mail')
            ]);
            redirect($request->_post('redirect'), 'gracias por suscribirte');
        }catch (\PDOException $e){
            redirect('/', 'Disculpe no se logro registrar su correo, posiblemente ya esta en nuetra base de datos');
        }

    }
}