<?php

namespace apps\Website\models;

use fw_Klipso\kernel\classes\abstracts\aModels;
use fw_Klipso\kernel\engine\dataBase\Constrainst;
use fw_Klipso\kernel\engine\dataBase\DataType;
use fw_Klipso\kernel\engine\dataBase\TypeFields;

class Suscripcion extends aModels
{
    private $prefix_model = '';

    public function __fields__()
    {
        $field = [
            'correo_electronico' => DataType::FieldString(100,true),
        ];
        return $field;
    }

    public function __setPrimary()
    { }

    public function __setUnique()
    {
        $uniq = [
            'correo_electronico'
        ];

        return $uniq;
    }

    public function __foreignKey()
    {
        /* It creates foreign keys, storing in an array variable each field that has relation to foreign models in
         * the following way.
         *
         * $fk = [
         *     'campo1' => Constrainst::ForeignKey('Name_of_foreign_model','Relational_field_of_the_foreign_model',Constrainst::on_delete(false)),
         *     'campo2' => Constrainst::ForeignKey('Name_of_foreign_model','Relational_field_of_the_foreign_model')
         * ];
         * return $fk;
         *
         */


    }
    public function __getPrefix()
    {
        return $this->prefix_model;
    }
}