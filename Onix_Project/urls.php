<?php
namespace onix_Project;

use fw_Klipso\kernel\engine\middleware;


$route = new  middleware\Urls();

/*
    Write each of the url, example
    $route->add('','website.WebsiteController.home');

    Website is the application name. WebsiteController is the filename php and the class name and home
    is the method that is invoked when the url is called from the browser

    In this example, the home of website is in the WebsiteController

    para que alguna parte de la url sea enviada como parametro al controlador debe ser con el siguiente formato de
        ejemplo P{([0-9]+)}
*/
#$route->add('','Landing.LandingController.index');
#$route->add('/home','Website.WebsiteController.home');
$route->add('','Website.WebsiteController.home');
$route->add('/info','Website.WebsiteController.info');
$route->add('/descarga','Website.WebsiteController.descargar');
$route->add('/comercio','Website.WebsiteController.comercio');
$route->add('/blog','Website.WebsiteController.blog');
$route->add('/pools','Website.WebsiteController.pools');
$route->add('/contacto','Website.WebsiteController.contacto');
$route->add('/exchanges','Website.WebsiteController.exchanges');
$route->add('/sesion','Website.WebsiteController.sesion');
$route->add('/registro','Website.WebsiteController.registro');
$route->add('/software','Website.WebsiteController.software');
$route->add('/road','Website.WebsiteController.road');
$route->add('/ficha','Website.WebsiteController.ficha');


require_once BASE_DIR . 'apps/Admin/urls.php';
require_once BASE_DIR . 'apps/Referidos/urls.php';


$route->submit();
