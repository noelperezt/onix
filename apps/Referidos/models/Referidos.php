<?php

namespace apps\Referidos\models;

use fw_Klipso\kernel\classes\abstracts\aModels;
use fw_Klipso\kernel\engine\dataBase\Constrainst;
use fw_Klipso\kernel\engine\dataBase\DataType;
use fw_Klipso\kernel\engine\dataBase\TypeFields;

class Referidos extends aModels
{
    private $prefix_model = '';

    public function __fields__()
    {
        $field  = [
            'correo_electronico' => DataType::FieldString(100, true),
            'clave' => DataType::FieldString(40, true),
            'referido' => DataType::FieldString(10,false),
            'codigo_referir' => DataType::FieldString(10)
            
        ];
        return $field;
    }

    public function __setPrimary()
    { }

    public function __setUnique()
    {
         $uniq = [
            'correo_electronico',
            'codigo_referir'
        ];

        return $uniq;
    }

    public function __foreignKey()
    {

    }
    public function __getPrefix()
    {
        return $this->prefix_model;
    }
}