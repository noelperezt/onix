<?php

namespace apps\Referidos\models;

use fw_Klipso\kernel\classes\abstracts\aModels;
use fw_Klipso\kernel\engine\dataBase\Constrainst;
use fw_Klipso\kernel\engine\dataBase\DataType;
use fw_Klipso\kernel\engine\dataBase\TypeFields;

class Onix_referidos extends aModels
{
    private $prefix_model = '';

    public function __fields__()
    {
        /*
        * Create a variable that stores an array with the fields that your model will have. Then returns that variable
        *
        * $field = [
        *    'campo1' => DataType::FieldString(200, true),
        *    'campo2' => DataType::FieldString(200, true),
        * ];
        *
        * return $field;
        *
        */
        $field = [
            'cantidad_onix' => DataType::FieldInteger(true, 2000000),
        ];

        return $field;
    }

    public function __setPrimary()
    {
        /* Create the primary key of your model by creating a variable that stores the field that will be PK. for example.
         * Then returns that variable
         *
         * $pk = [
         *     'campo1'
         * ];
         *
         * return $pk;
         *
         */

    }

    public function __setUnique()
    {
        /* Create unique fields for your model by creating a variable that stores those cmpos. for example.
         * Then returns that variable
         *
         * $uniq = [
         *     'campo1'
         * ];
         *
         * return $uniq;
         *
         */
    }

    public function __foreignKey()
    {
        /* It creates foreign keys, storing in an array variable each field that has relation to foreign models in
         * the following way.
         *
         * $fk = [
         *     'campo1' => Constrainst::ForeignKey('Name_of_foreign_model','Relational_field_of_the_foreign_model',Constrainst::on_delete(false)),
         *     'campo2' => Constrainst::ForeignKey('Name_of_foreign_model','Relational_field_of_the_foreign_model')
         * ];
         * return $fk;
         *
         */


    }
    public function __getPrefix()
    {
        return $this->prefix_model;
    }
}