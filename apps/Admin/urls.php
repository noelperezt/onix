<?php
global $route;
use fw_Klipso\kernel\engine\middleware\Session;

$route->add('/admin','Admin.AdminController.login');
$route->add('/admin/salir', 'Admin.AdminController.cerrar_sesion');

$route->add( Session::loginRequired('/admin/dashboard/'),'Admin.AdminController.dashboard');
$route->add( Session::loginRequired('/admin/usuario-listado'), 'Admin.AdminController.usuario_listado');
$route->add( Session::loginRequired('/admin/usuario'), 'Admin.AdminController.usuario');
$route->add( Session::loginRequired('/admin/usuario/editar/P{([0-9]+)}'), 'Admin.AdminController.usuario_editar');
$route->add( Session::loginRequired('/admin/post-listado'), 'Admin.AdminController.post_listado');
$route->add( Session::loginRequired('/admin/post'), 'Admin.AdminController.post_crear');
$route->add( Session::loginRequired('/admin/categorias'), 'Admin.AdminController.categorias_crear');
$route->add( Session::loginRequired('/admin/multimedia/listado'), 'Admin.AdminController.multimedia_listado');

$route->add( Session::loginRequired('/admin/post/upload-image/'), 'Admin.AdminController.subir_archivos');