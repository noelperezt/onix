<?php

namespace apps\Admin\models;

use fw_Klipso\kernel\classes\abstracts\aModels;
use fw_Klipso\kernel\engine\dataBase\DataType;
use fw_Klipso\kernel\engine\dataBase\TypeFields;

class Categoria extends aModels
{
    private $prefix_model = 'blog';

    public function __fields__()
    {
        $field = [
            'categoria_id' => DataType::FieldAutoField(),
            'nombre_categoria' => DataType::FieldString(40, true),
            'url_categoria' => DataType::FieldString(40, true),
            'estado' => DataType::FieldChar(true,'A')
            
        ];
        return $field;
    }

    public function __setPrimary()
    {
        $pk = [
            'categoria_id'
        ];
        return $pk;

    }

    public function __setUnique(){
        $uniq = [
            'url_categoria' ,'nombre_categoria'
        ];
        return $uniq;
    }

    public function __foreignKey(){
        
    }
    public function __getPrefix()
    {
        return $this->prefix_model;
    }
}